from flask import Flask, render_template, redirect, request, flash


users = {"usuario.teste":"teste@123","admin":"admin"}

app = Flask(__name__)
app.config['SECRET_KEY'] = 'teste@123'

@app.route('/')
def home():
    return render_template('login.html')


@app.route('/login', methods=['POST'])
def login():


    nome = request.form.get('nome')
    senha = request.form.get('senha')

    if nome in users and users[nome] == senha:
        return render_template("home.html"), 200
    else:
        return "Senha/Login incorreto!", 403
    

@app.route('/health-check', methods=['GET'])
def healthCheck():
    return 'Status Code: 200'
    

if __name__ in "__main__":
    app.run()    # debug=True
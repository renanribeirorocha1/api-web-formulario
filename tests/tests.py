import unittest
from flask import request
from app import app

class AppTestCase(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()

    def tearDown(self):
        pass

    def test_home(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)

    def test_login_success(self):
        response = self.app.post('/login', data=dict(nome='usuario.teste', senha='teste@123'))
        self.assertEqual(response.status_code, 200)

    def test_login_wrong_password(self):
        response = self.app.post('/login', data=dict(nome='usuario.teste', senha='wrongpassword'))
        self.assertEqual(response.status_code, 403)

    def test_login_wrong_username(self):
        response = self.app.post('/login', data=dict(nome='wrongusername', senha='teste@123'))
        self.assertEqual(response.status_code, 403)

    def test_login_missing_credentials(self):
        response = self.app.post('/login', data=dict(nome='', senha=''))
        self.assertEqual(response.status_code, 403)

if __name__ == '__main__':
    unittest.main()



    


